import React from "react";
import { Button, Space, Upload, Popconfirm, Input, Alert } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import AdminLayout from "../../../components/Layouts/AdminLayout.jsx";
import { useEffect, useState } from "react";

export default function ObjectCreateView() {
    const [name, setName] = useState("");
    const [address, setAddress] = useState("");
    const [city, setCity] = useState("Нур-Султан");
    const [country, setCountry] = useState("Казахстан");
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState("");
    const [alertType, setAlertType] = useState("success");
    const [objects, setObjects] = useState([]);
    const [objectsToShow, setObjectsToShow] = useState([]);

    const handleNameChange = (e) => setName(e.target.value);
    const handleAddressChange = (e) => setAddress(e.target.value);
    const handleCityChange = (e) => setCity(e.target.value);
    const handleCountyChange = (e) => setCountry(e.target.value);

    useEffect(() => {
        const _obj = objects.filter((a) => {
            // console.log(a.address, typeof a.address);
            const _n = name
                ? a.name.toString().toLowerCase().includes(name.toLowerCase())
                : address
                ? false
                : true;
            const _adr = address
                ? a.address
                      .toString()
                      .toLowerCase()
                      .includes(address.toLowerCase())
                : name
                ? false
                : true;
            // console.log("sorting");
            return _n | _adr;
        });
        setObjectsToShow(_obj);
    }, [objects, name, address]);

    useEffect(async () => {
        const response = await fetch("http://localhost:3000/api/buildings");
        const objectsJson = await response.json();
        const { result } = objectsJson;
        console.log(result);
        setObjects([...result]);
    }, []);

    useEffect(async () => {
        if (showAlert === true) {
            setTimeout(() => {
                setShowAlert(false);
            }, 5000);
        }
    }, [showAlert]);

    return (
        <>
            <h3>Создание</h3>
            <div
                style={{
                    display: "grid",
                    gridTemplateColumns: "2fr 1fr 1fr",
                    height: "100%",
                }}
            >
                <div
                    style={{
                        height: "100%",
                    }}
                >
                    <Space direction="vertical">
                        <Input
                            addonBefore="Название ЖК"
                            value={name}
                            onChange={handleNameChange}
                        />
                        <Input
                            addonBefore="Адрес"
                            value={address}
                            onChange={handleAddressChange}
                        />
                        <Input
                            addonBefore="Город"
                            value={city}
                            onChange={handleCityChange}
                        />
                        <Input
                            addonBefore="Казахстан"
                            value={country}
                            onChange={handleCountyChange}
                        />
                        <Button
                            onClick={async () => {
                                const resp = await fetch(
                                    "http://localhost:3000/api/buildings/new",
                                    {
                                        method: "POST",
                                        headers: {
                                            "Content-Type": "application/json",
                                        },
                                        body: JSON.stringify({
                                            name,
                                            address,
                                            city,
                                            country,
                                            images: [],
                                        }),
                                    }
                                );
                                const answer = await resp.json();
                                if (resp.status === 200) {
                                    setName("");
                                    setAddress("");
                                    setAlertMessage(answer.message);
                                    setAlertType("success");
                                    // setName("")
                                } else {
                                    setAlertMessage(
                                        answer.message
                                            ? answer.message
                                            : "error"
                                    );
                                    setAlertType("error");
                                }
                                setShowAlert(true);
                            }}
                        >
                            Создать
                        </Button>
                        {showAlert && (
                            <Alert message={alertMessage} type={alertType} />
                        )}
                    </Space>
                </div>
                <div
                    style={{
                        overflowY: "scroll",
                        height: "100%",
                    }}
                >
                    {objectsToShow &&
                        objectsToShow.map((obj) => {
                            return (
                                <div
                                    key={obj._id}
                                    style={{
                                        border: "1px solid black",
                                        padding: 3,
                                        margin: 3,
                                    }}
                                >
                                    <h3>{obj.name}</h3>
                                    <p>{obj.address}</p>
                                    <p>{obj.city}</p>
                                    <p>{obj._id}</p>
                                </div>
                            );
                        })}
                </div>
                <div>
                    <ul>
                        <li>Начните вводить название или адрес</li>
                        <li>
                            Используйте короткие части имени (компьютер не умеет
                            находить опечатки)
                        </li>
                        <li>
                            Если объект уже есть в списке, не создавайте второй
                            такойже объект
                        </li>
                        <li>
                            Сохраните id (длинный номер снизу), чтобы добавить
                            остальные объекты к нему
                        </li>
                    </ul>
                </div>
            </div>
        </>
    );
}
