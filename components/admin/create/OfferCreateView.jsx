import React from "react";
import {
    Button,
    Space,
    Upload,
    Popconfirm,
    Input,
    Alert,
    Radio,
    InputNumber,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import AdminLayout from "../../Layouts/AdminLayout.jsx";
import { useEffect, useState } from "react";

const { TextArea } = Input;

export default function OfferCreateView() {
    const [type, setType] = useState("PARKING"); // // PARKING, GARAGE, PANTRY
    const [name, setName] = useState("");
    const [nameToFind, setNameToFind] = useState("");
    const [description, setDescription] = useState("");
    const [totalLevel, setTotalLevel] = useState(1);
    const [level, setLevel] = useState(1);
    const [priceBasic, setPriceBasic] = useState(1000000);
    const [nameInternal, setNameInternal] = useState("");
    const [images, setImages] = useState("");

    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState("");
    const [alertType, setAlertType] = useState("success");
    const [objects, setObjects] = useState([]);
    const [objectsToShow, setObjectsToShow] = useState([]);
    const [objectId, setObjectId] = useState("");
    const [objectFound, setObjectFound] = useState(null);

    const handleNameChange = (e) => setName(e.target.value);
    const handleNameToFindChange = (e) => setNameToFind(e.target.value);
    const handleDescriptionChange = (e) => setDescription(e.target.value);
    const handleTotalLevelChange = (value) => setTotalLevel(Math.round(value));
    const handleLevelChange = (value) => setLevel(Math.round(value));
    const handlePriceBasicChange = (value) => setPriceBasic(Math.round(value));
    const handleObjectIdChange = (e) => {
        setObjectId(e.target.value);
        setObjectFound(null);
    };
    const handleNameInternalChange = (e) => setNameInternal(e.target.value);
    const handleImagesChange = (e) => setImages(e.target.value);

    useEffect(() => {
        const _obj = objects.filter((a) => {
            // console.log(a.address, typeof a.address);
            const _n = nameToFind
                ? a.name
                      .toString()
                      .toLowerCase()
                      .includes(nameToFind.toLowerCase())
                : true;

            return _n;
        });
        setObjectsToShow(_obj);
    }, [objects, nameToFind]);

    useEffect(async () => {
        const response = await fetch("http://localhost:3000/api/buildings");
        const objectsJson = await response.json();
        const { result } = objectsJson;
        console.log(result);
        setObjects([...result]);
    }, []);

    useEffect(async () => {
        if (showAlert === true) {
            setTimeout(() => {
                setShowAlert(false);
            }, 5000);
        }
    }, [showAlert]);

    const changeType = (e) => {
        setType(e.target.value);
    };
    const typeOptions = [
        { label: "Паркинг", value: "PARKING" },
        { label: "Гараж", value: "GARAGE" },
        { label: "Кладовка", value: "PANTRY" },
    ];

    return (
        <>
            <h3>Создание</h3>
            <div
                style={{
                    display: "grid",
                    gridTemplateColumns: "2fr 1fr 1fr",
                    height: "100%",
                }}
            >
                <div
                    style={{
                        height: "100%",
                        overflowY: "scroll",
                        paddingRight: 10,
                    }}
                >
                    <Space direction="vertical">
                        <Input
                            addonBefore="ID объекта"
                            value={objectId}
                            onChange={handleObjectIdChange}
                        />
                        <Button
                            onClick={async () => {
                                const resp = await fetch(
                                    "http://localhost:3000/api/buildings/"
                                );
                                const answer = await resp.json();
                                if (resp.status === 200) {
                                    setAlertMessage(answer.message);
                                    setAlertType("success");
                                    // setName("")
                                    const isObjectExist = await answer.result.find(
                                        (a) => a._id === objectId
                                    );
                                    if (isObjectExist) {
                                        setObjectFound(isObjectExist);
                                    } else {
                                        setObjectFound(null);
                                    }
                                } else {
                                    setAlertMessage(
                                        answer.message
                                            ? answer.message
                                            : "error"
                                    );
                                    setAlertType("error");
                                    setObjectFound(null);
                                }
                                setShowAlert(true);
                            }}
                        >
                            Проверить ID
                        </Button>

                        {objectFound && (
                            <div>
                                <b>{objectFound.name}</b>
                                <p>{objectFound.address}</p>
                                <p>{objectFound._id}</p>
                            </div>
                        )}
                        <Radio.Group
                            options={typeOptions}
                            onChange={changeType}
                            value={type}
                            optionType="button"
                            buttonStyle="solid"
                        />

                        <Input
                            addonBefore="Короткое Название"
                            value={name}
                            onChange={handleNameChange}
                        />
                        <p>
                            Укажите в поле короткое название: номер парковочного
                            места, номер гаража, номер квартиры, рядом с которой
                            находится кладовка
                        </p>
                        <h3>Описание</h3>
                        <TextArea
                            rows={4}
                            showCount
                            maxLength={250}
                            addonBefore="Описание"
                            value={description}
                            onChange={handleDescriptionChange}
                            multiple={true}
                        />

                        <h3>Всего уровней / этажей</h3>

                        <InputNumber
                            // min={1}
                            addonBefore="Всего уровней / этажей"
                            value={totalLevel}
                            onChange={handleTotalLevelChange}
                        />
                        <p>
                            Укажите количество уровней или этажей в здании. (Для
                            парковок, укажите только уровни парковки (не включая
                            этажи жилого дома). Для одноэтажных гаражей на земле
                            укажите 1). Для кладовки внутри жилого дома, укажите
                            количество этажей жилого дома
                        </p>
                        <h3>Этаж</h3>

                        <InputNumber
                            // min={1}
                            addonBefore="Всего уровней / этажей"
                            value={level}
                            onChange={handleLevelChange}
                        />
                        {level > totalLevel && (
                            <p style={{ color: "red" }}>
                                Не может быть больше общего количества этажей
                            </p>
                        )}
                        <p>
                            Укажите уровнь или этаж в здании на котором
                            находится объект
                        </p>

                        <h3>Базовая Цена / Цена На Сайте</h3>
                        <InputNumber
                            // min={1}
                            addonBefore="Базовая Цена / Цена на сайте"
                            value={priceBasic}
                            onChange={handlePriceBasicChange}
                        />

                        <p>
                            Цена которая будет отображаться на сайте. Клиент
                            получит сумму ниже, после вычета нашей коммиссии
                        </p>

                        <h3>Заметка (непубличное поле)</h3>
                        <TextArea
                            // min={1}
                            rows={4}
                            addonBefore="Заметка (непубличное поле)"
                            value={nameInternal}
                            onChange={handleNameInternalChange}
                        />

                        <p>
                            Укажите заметку, которая будет видна только в режиме
                            редактирования объявления
                        </p>

                        <h3>Ссылки на картинки</h3>
                        <TextArea
                            // min={1}
                            rows={4}
                            addonBefore="Ссылки на картинки"
                            value={images}
                            onChange={handleImagesChange}
                        />

                        <p>
                            Укажите ссылку на фото или картинку. Новое фото -
                            новая строка
                        </p>

                        <Button
                            type="primary"
                            disabled={objectFound ? false : true}
                            onClick={async () => {
                                const imagesArray = images.trim().split("\n");
                                const imagesToSend = [];
                                for (let i in imagesArray) {
                                    imagesToSend.push({
                                        image: imagesArray[i],
                                        type: "photo",
                                    });
                                }
                                console.log("imagesArray", imagesArray);
                                // return;
                                const resp = await fetch(
                                    "http://localhost:3000/api/offer/new",
                                    {
                                        method: "POST",
                                        headers: {
                                            "Content-Type": "application/json",
                                        },
                                        body: JSON.stringify({
                                            objectId,
                                            name,
                                            description,
                                            type,
                                            priceBasic,
                                            nameInternal,
                                            level,
                                            totalLevel,
                                            images: imagesToSend,
                                        }),
                                    }
                                );
                                const answer = await resp.json();
                                if (resp.status === 200) {
                                    // setName("");
                                    // setAddress("");
                                    setAlertMessage(answer.message);
                                    setAlertType("success");
                                    // setName("")
                                } else {
                                    setAlertMessage(
                                        answer.message
                                            ? answer.message
                                            : "error"
                                    );
                                    setAlertType("error");
                                }
                                setShowAlert(true);
                            }}
                        >
                            Создать
                        </Button>
                        {showAlert && (
                            <Alert message={alertMessage} type={alertType} />
                        )}
                    </Space>
                </div>
                <div
                    style={{
                        overflowY: "scroll",
                        height: "100%",
                    }}
                >
                    <Input
                        addonBefore="ЖК или адрес"
                        value={nameToFind}
                        onChange={handleNameToFindChange}
                    />
                    {objectsToShow &&
                        objectsToShow.map((obj) => {
                            return (
                                <div
                                    key={obj._id}
                                    style={{
                                        border: "1px solid black",
                                        padding: 3,
                                        margin: 3,
                                    }}
                                >
                                    <h3>{obj.name}</h3>
                                    <p>{obj.address}</p>
                                    <p>{obj.city}</p>
                                    <input readOnly value={obj._id}></input>
                                </div>
                            );
                        })}
                </div>
                <div>
                    <ul>
                        <li>Начните вводить название или адрес</li>
                        <li>
                            Используйте короткие части имени (компьютер не умеет
                            находить опечатки)
                        </li>
                        <li>
                            Если объект уже есть в списке, не создавайте второй
                            такойже объект
                        </li>
                        <li>
                            Сохраните id (длинный номер снизу), чтобы добавить
                            остальные объекты к нему
                        </li>
                    </ul>
                </div>
            </div>
        </>
    );
}
