import React from "react";

export default function ParkingCardDetails({ text1, text2, switcher }) {
    return (
        <div
            style={{
                width: "85%",
            }}
        >
            {switcher ? (
                <mark
                    style={{
                        backgroundColor: "white",
                        fontSize: "2.3rem",
                    }}
                >
                    <b>{text1}</b>

                    <br />
                    {text2}
                </mark>
            ) : (
                <mark
                    style={{
                        backgroundColor: "rgba(255,255,255,0.1)",
                        fontSize: "1.3rem",
                    }}
                >
                    {text1}
                    {/* <b>{text1}</b> {text2} */}
                </mark>
            )}
        </div>
    );
}
