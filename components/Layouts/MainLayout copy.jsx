import Head from "next/head";
// =========================================
import { Layout, Input, Space } from "antd";
import "antd/dist/antd.css";
import MainHeader from "../pageElements/MainHeader";
// =========================================

const { Header, Footer, Content } = Layout;
const { Search } = Input;

export default function MainLayout({ children }) {
    return (
        <>
            <Head>
                <title>Parkingi.kz - Все паркинги Нур-Султана</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Layout>
                <MainHeader />
                <Content>{children}</Content>
                <Footer>Footer</Footer>
            </Layout>
        </>
    );
}
