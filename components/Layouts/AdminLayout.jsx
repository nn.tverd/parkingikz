import { Layout, Menu, Breadcrumb } from "antd";
import {
    UserOutlined,
    LaptopOutlined,
    NotificationOutlined,
    HomeOutlined,
    FileAddOutlined,
} from "@ant-design/icons";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import Link from "next/link";

import GS from "../../store/globalStore";
import { useRouter } from "next/router";

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

const TopMenu = () => {
    return (
        <Menu
            theme="dark"
            mode="horizontal"
            // defaultSelectedKeys={["2"]}
        >
            <Menu.Item key="1">
                <Link href="/css/div/my">Заявки</Link>
            </Menu.Item>
            <Menu.Item key="2">
                <Link href="/css/div/moder">Модерация объявлений</Link>
            </Menu.Item>
            <Menu.Item key="3">
                <Link href="/css/div/create">Создание сущностей</Link>
            </Menu.Item>
        </Menu>
    );
};

const SubMenuCreation = ({ select }) => {
    return (
        <>
            <Menu
                mode="inline"
                defaultSelectedKeys={["1"]}
                // defaultOpenKeys={["sub1"]}
                style={{ height: "100%", borderRight: 0 }}
            >
                <Menu.Item
                    key="1"
                    icon={<HomeOutlined />}
                    onClick={() => {
                        select("OBJECT");
                    }}
                >
                    Объект (ЖК, адрес)
                </Menu.Item>
                <Menu.Item
                    key="2"
                    icon={<FileAddOutlined />}
                    onClick={() => {
                        select("OFFER");
                    }}
                >
                    Предложения (Помещения)
                </Menu.Item>
            </Menu>
        </>
    );
};

export default function adminPanel({ children }) {
    const { adminNavigationStore } = GS;
    const { selectCreate, selectModer } = adminNavigationStore;
    const router = useRouter();
    console.log(router);
    const isCreateSub = router.route.includes("/css/div/create");
    const isModerSub = router.route.includes("/css/div/moder");
    return (
        <div
            style={{
                height: "100vh",
            }}
        >
            <Layout
                style={{
                    height: "100vh",
                }}
            >
                <Header className="header">
                    <div className="logo" />
                    <TopMenu />
                </Header>
                <Layout>
                    <Sider width={200} className="site-layout-background">
                        {isCreateSub && (
                            <SubMenuCreation select={selectCreate} />
                        )}
                        {isModerSub && <SubMenuCreation select={selectModer} />}
                    </Sider>
                    <Layout style={{ padding: "0 24px 24px" }}>
                        {/* <Breadcrumb style={{ margin: "16px 0" }}>
                            <Breadcrumb.Item>Home</Breadcrumb.Item>
                            <Breadcrumb.Item>List</Breadcrumb.Item>
                            <Breadcrumb.Item>App</Breadcrumb.Item>
                        </Breadcrumb> */}
                        <Content
                            className="site-layout-background"
                            style={{
                                padding: 24,
                                margin: 0,
                                minHeight: 280,
                            }}
                        >
                            {children}
                        </Content>
                    </Layout>
                </Layout>
            </Layout>
        </div>
    );
}
