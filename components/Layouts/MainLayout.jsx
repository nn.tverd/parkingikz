import Head from "next/head";
// =========================================
// import { Layout, Input, Space } from "antd";
import "antd/dist/antd.css";
import MainHeader from "../pageElements/MainHeader";
import { useEffect } from "react";
// import ListSideBar from "../pageElements/ListSideBar";
// =========================================

// const { Header, Footer, Content } = Layout;
// const { Search } = Input;

export default function MainLayout({ children }) {
    useEffect(() => {
        let vh = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty("--vh", `${vh}px`);
        const appHeight = () =>
            document.documentElement.style.setProperty(
                "--app-height",
                `${window.innerHeight}px`
            );
        window.addEventListener("resize", appHeight);
        appHeight();
    }, []);
    return (
        <>
            <Head>
                <title>Parkingi.kz - Все паркинги Нур-Султана</title>
                <link rel="icon" href="/favicon.ico" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
                />
                <meta name="google" content="notranslate" />
                {/* <meta name="theme-color" content="#000000" /> */}
                <meta name="description" content="Купить паркинг легко" />

                {/* <link
                    rel="apple-touch-icon"
                    href="%PUBLIC_URL%/images/icons/icon-192x192.png"
                /> */}

                <title>Parkingi.kz | Выбрать паркинг легко</title>
                {/* <meta
                    name="og:image"
                    content="/images/icons/favicon_io/apple-touch-icon.png"
                />
                <meta
                    property="og:image"
                    content="/images/icons/favicon_io/apple-touch-icon.png"
                /> */}
            </Head>
            <div
                style={{
                    width: "100%",
                    maxWidth: "720px",
                    margin: "0 auto",
                    maxHeight: "calc( 100vh - 60px )",
                    // height: "calc( 100vh - 60px )",
                    // height: "100vh",
                    height: "calc(var(--app-height) - 50px)",
                    // borderRadius: " 10px",
                    backgroundColor: "black",
                    // position: "rere",
                    scrollSnapType: "y mandatory",
                    overflowY: "scroll",
                }}
            >
                {children}
            </div>
            <MainHeader />
        </>
    );
}
