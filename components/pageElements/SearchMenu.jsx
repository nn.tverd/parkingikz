import React, { useState } from "react";
// import useRouter from "next/router";
// import Link from "next/link";
import { useRouter } from "next/router";
import {
    Layout,
    Input,
    Space,
    Tooltip,
    Button,
    Divider,
    Card,
    Slider,
    InputNumber,
} from "antd";
const { Search } = Input;

export default function SearchMenu({ setShowSearch }) {
    const router = useRouter();
    const [query, setQuery] = useState("");
    const [selectedType, setSelectedType] = useState({
        // all: false,
        parking: true,
        garage: false,
        pantry: false,
    });
    const __minPrice = 0;
    const [minPrice, setMinPrice] = useState(__minPrice);
    const __maxPrice = 50000000;
    const [maxPrice, setMaxPrice] = useState(__maxPrice);

    const [sliderPosition, setSliderPosition] = useState([
        __minPrice,
        __maxPrice,
    ]);
    const [selectedMoney, setSelectedMoney] = useState({
        cash: true,
        installment: true,
        loan: false,
    });

    const [selectedModeration, setSelectedModeration] = useState({
        all: true,
        only: false,
    });

    const changeType = (type) => {
        const value = selectedType;
        value[type] = !value[type];
        setSelectedType({ ...value });
    };
    const changeMoney = (type) => {
        const value = selectedMoney;
        value[type] = !value[type];
        setSelectedMoney({ ...value });
    };
    const changeModerated = () => {
        const value = selectedModeration;
        const newVal = value.all;
        value.all = !!!newVal;
        value.only = !!newVal;
        setSelectedModeration({ ...value });
    };

    const logScale = (val) => {
        const value =
            (val * val * val * val) / (__maxPrice * __maxPrice * __maxPrice);

        return value.toFixed(0);
    };

    const onSearch = () => {
        let endpoint = "/all?";
        endpoint += `query=${query}`;
        //
        endpoint += `&parking=${selectedType.parking}`;
        endpoint += `&garage=${selectedType.garage}`;
        endpoint += `&pantry=${selectedType.pantry}`;
        //
        endpoint += `&minPrice=${minPrice}`;
        endpoint += `&maxPrice=${maxPrice}`;
        //
        endpoint += `&cash=${selectedMoney.cash}`;
        endpoint += `&installment=${selectedMoney.installment}`;
        endpoint += `&loan=${selectedMoney.loan}`;
        //
        endpoint += `&all=${selectedModeration.all}`;
        endpoint += `&only=${selectedModeration.only}`;

        router.push(endpoint);
    };
    return (
        <div
            style={{
                position: "absolute",
                width: "95vw",
                height: "100vh",
                background: "whitesmoke",
                left: 0,
                top: 0,
                padding: 5,
                overflowY: "scroll",
                zIndex: 3,
            }}
        >
            <div
                style={{
                    width: "90vw",
                    display: "grid",
                    justifyContent: "end",
                    padding: 5,
                }}
            >
                <Button
                    onClick={() => {
                        setShowSearch(false);
                    }}
                >
                    Close
                </Button>
            </div>

            <div>
                <h2>Поиск</h2>
            </div>
            <div>
                <Search
                    placeholder="Адрес или Название Жилого Комплекса"
                    onSearch={onSearch}
                    enterButton
                    value={query}
                    onChange={(e) => {
                        setQuery(e.target.value);
                    }}
                />
            </div>
            {/** // type */}
            <div>
                {/* <h3>Тип оsбъекта</h3> */}
                <Card style={{ width: "100%" }}>
                    <Divider orientation="left">Тип объекта</Divider>
                    {/* <Button
                        type={selectedType.all && "primary"}
                        onClick={() => {
                            changeType("all");
                        }}
                    >
                        Все
                    </Button> */}
                    <Button
                        type={selectedType.parking && "primary"}
                        onClick={() => {
                            changeType("parking");
                        }}
                    >
                        Паркинг
                    </Button>
                    <Button
                        type={selectedType.garage && "primary"}
                        onClick={() => {
                            changeType("garage");
                        }}
                    >
                        Гараж
                    </Button>
                    <Button
                        type={selectedType.pantry && "primary"}
                        onClick={() => {
                            changeType("pantry");
                        }}
                    >
                        Кладовка
                    </Button>
                </Card>
            </div>
            {/*  // price */}
            <div>
                <Card style={{ width: "100%" }}>
                    <Divider orientation="left">Цена</Divider>

                    <div
                        style={{
                            display: "grid",
                            gridTemplateColumns: "1fr 1fr",
                            // placeItems: "center",
                        }}
                    >
                        <div style={{ justifySelf: "start" }}>
                            <InputNumber
                                style={{
                                    width: "90%",
                                }}
                                min={__minPrice}
                                max={maxPrice - 1000}
                                step={10000}
                                // style={{ margin: "0 16px" }}
                                value={minPrice}
                                formatter={(value) => {
                                    const valueStr = `${value}`;
                                    const newVal = valueStr.replace(
                                        /(\d)(?=(\d{3})+(\.(\d){0,2})*$)/g,
                                        "$1 "
                                    );
                                    // console.log("newVal", newVal);
                                    return newVal + " тг";
                                }}
                                parser={(value) => {
                                    value = value.replace(",", "");
                                    value = value.replace(" тг", "");
                                    return value;
                                }}
                                onChange={(value) => {
                                    setMinPrice(value);
                                }}
                            />
                        </div>
                        <div style={{ justifySelf: "end" }}>
                            <InputNumber
                                style={{
                                    width: "90%",
                                }}
                                min={minPrice + 1000}
                                max={__maxPrice}
                                step={10000}
                                // style={{ margin: "0 16px" }}
                                formatter={(value) => {
                                    const valueStr = `${value}`;
                                    const newVal = valueStr.replace(
                                        /(\d)(?=(\d{3})+(\.(\d){0,2})*$)/g,
                                        "$1 "
                                    );
                                    // console.log("newVal", newVal);
                                    return newVal + " тг";
                                }}
                                parser={(value) => {
                                    value = value.replace(",", "");
                                    value = value.replace(" тг", "");
                                    return value;
                                }}
                                value={maxPrice}
                                onChange={(value) => {
                                    setMaxPrice(value);
                                }} // onChange={this.onChange}
                            />
                        </div>
                    </div>
                    <Slider
                        range
                        min={__minPrice}
                        max={__maxPrice}
                        step={10000}
                        value={sliderPosition}
                        onChange={(value) => {
                            // console.log(value);
                            setSliderPosition(value);
                            setMinPrice(logScale(value[0]));
                            setMaxPrice(logScale(value[1]));
                        }}
                        tipFormatter={null}
                    />
                </Card>
            </div>
            {/* kredit rassrochka  */}
            <div>
                <Card style={{ width: "100%" }}>
                    <Divider orientation="left">Способ покупки</Divider>
                    {/* <Button
                        type={selectedType.all && "primary"}
                        onClick={() => {
                            changeType("all");
                        }}
                    >
                        Все
                    </Button> */}
                    <Button
                        type={selectedMoney.cash && "primary"}
                        onClick={() => {
                            changeMoney("cash");
                        }}
                    >
                        Наличные
                    </Button>
                    <Button
                        type={selectedMoney.installment && "primary"}
                        onClick={() => {
                            changeMoney("installment");
                        }}
                    >
                        Рассрочка
                    </Button>
                    <Button
                        type={selectedMoney.loan && "primary"}
                        onClick={() => {
                            changeMoney("loan");
                        }}
                    >
                        Кредит
                    </Button>
                </Card>
            </div>
            {/* proverennye i net */}
            <div>
                <Card style={{ width: "100%" }}>
                    <Divider orientation="left">Модерация</Divider>
                    {/* <Button
                        type={selectedType.all && "primary"}
                        onClick={() => {
                            changeType("all");
                        }}
                    >
                        Все
                    </Button> */}
                    <Button
                        type={selectedModeration.all && "primary"}
                        onClick={() => {
                            changeModerated("all");
                        }}
                    >
                        Все
                    </Button>
                    <Button
                        type={selectedModeration.only && "primary"}
                        onClick={() => {
                            changeModerated("only");
                        }}
                    >
                        Проверенные
                    </Button>
                </Card>
            </div>
            {/* nachat poisk */}
            <div>
                <Card style={{ width: "100%" }}>
                    <Divider orientation="left">Начать поиск</Divider>
                    <Button danger type={"primary"} onClick={() => {}}>
                        Поиск
                    </Button>
                </Card>
            </div>
            {/* pustota */}
            <div style={{ minHeight: "50vh" }}></div>
        </div>
    );
}
