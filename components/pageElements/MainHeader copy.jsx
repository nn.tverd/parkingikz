import React, { useState } from "react";
import { Layout, Input, Space, Tooltip, Button } from "antd";
import { List, Typography, Divider } from "antd";
import { SearchOutlined, BarsOutlined } from "@ant-design/icons";
import SearchMenu from "./SearchMenu";
import NavMenu from "./NavMenu";
// import { ReactComponent as ReactLogo } from "../../public/some.svg";
// import Logo from "../../public/some.svg";
const { Header, Footer, Content } = Layout;
// const { Search } = Input;

export default function MainHeader() {
    const [showSearch, setShowSearch] = useState(false);
    const [showMenu, setShowMenu] = useState(false);
    return (
        <Header
            style={{
                // background: "none",
                // position: "fixed",
                zIndex: 1,
                width: "100%",
            }}
        >
            <div
                style={{
                    display: "grid",
                    alignItems: "center",
                    minHeight: "100%",
                    gridTemplateColumns: "auto 80px",
                }}
            >
                <div
                    style={{
                        display: "grid",
                        alignItems: "center",
                        minHeight: "100%",
                        color: "white",
                    }}
                >
                    <img src="/some.svg" alt="logo" />
                </div>
                <div
                    style={{
                        display: "grid",
                        // alignItems: "",
                        alignContent: "center",
                        justifyContent: "space-around",
                        minHeight: "100%",
                        gridTemplateColumns: "1fr 1fr 3fr",
                        color: "white",
                    }}
                >
                    <div>
                        <Tooltip title="search">
                            <Button
                                shape="circle"
                                icon={<SearchOutlined />}
                                onClick={() => setShowSearch(true)}
                            />
                        </Tooltip>
                    </div>
                    <div style={{}}></div>
                    <div>
                        <Tooltip title="menu">
                            <Button
                                shape="circle"
                                icon={<BarsOutlined />}
                                onClick={() => setShowMenu(true)}
                            />
                        </Tooltip>
                    </div>
                </div>
            </div>
            {showSearch && <SearchMenu setShowSearch={setShowSearch} />}
            {showMenu && <NavMenu setShowMenu={setShowMenu} />}
        </Header>
    );
}
