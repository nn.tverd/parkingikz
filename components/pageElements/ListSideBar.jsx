import React, { useEffect } from "react";
import { Layout, Input, Space, Tooltip, Button } from "antd";
import {
    SearchOutlined,
    ZoomInOutlined,
    HomeOutlined,
    DollarOutlined,
    EnvironmentOutlined,
    GoldOutlined,
    FullscreenOutlined,
    DislikeOutlined,
    HeartOutlined,
    CheckCircleOutlined,
    MoreOutlined,
} from "@ant-design/icons";
import globalStore from "../../store/globalStore";

const SideBarButton = ({ fontColor, bgColor, title, show, hide, Icon }) => {
    console.log(fontColor);
    const btnStyle = {
        color: fontColor,
        backgroundColor: bgColor,
        borderColor: bgColor,
    };

    return (
        <div
            style={{
                display: "grid",
                placeItems: "center",
                gridTemplateColumns: "1fr",
            }}
        >
            <Button
                shape="round"
                size="large"
                style={btnStyle}
                icon={<Icon />}
                // onClick={() => setShowSearch(true)}
                onTouchStart={() => show()}
                onMouseDown={() => show()}
                onTouchEnd={() => hide()}
                onMouseUp={() => {
                    hide();
                }}
            />
            <div
                style={{
                    display: "grid",
                    placeItems: "center",
                    width: "100%",
                }}
            >
                <strong className="tiktok-glithc-h1">{title}</strong>
                {/* <strong >{title}</strong> */}
            </div>
        </div>
    );
};

export default function ListSideBar() {
    const {
        showPrice,
        hidePrice,
        showAddress,
        hideAddress,
        showSize,
        hideSize,
        // showHowMany,
        // hideHowMany,
        changeShowMore,
    } = globalStore.sideBarStore;

    useEffect(() => {
        // console.log("side bar use effect");
        // const test = window.sessionStorage.getItem("test") || 0;
        // console.log(test);
        // window.sessionStorage.setItem("test", Number(test) + 1);
    }, []);
    return (
        <div
            style={{
                position: "absolute",
                right: 10,
                top: 50,
                height: "80%",
                // backgroundColor: "red",
                display: "grid",
                gridTemplateColumns: "1fr",
                placeItems: "center",
                alignContent: "start",
                gridGap: 10,
                zIndex: 1,
                overflowY: "scroll",
                scrollbarWidth: "none",
            }}
        >
            <SideBarButton
                fontColor="white"
                bgColor="black"
                title="Адрес"
                show={showAddress}
                hide={hideAddress}
                Icon={EnvironmentOutlined}
            />
            <SideBarButton
                fontColor="white"
                bgColor="black"
                title="Цена"
                show={showPrice}
                hide={hidePrice}
                Icon={DollarOutlined}
            />
            <SideBarButton
                fontColor="white"
                bgColor="black"
                title="Размеры"
                show={showSize}
                hide={hideSize}
                Icon={FullscreenOutlined}
            />
            <SideBarButton
                fontColor="white"
                bgColor="black"
                title="Ещё"
                show={changeShowMore}
                hide={() => {}}
                Icon={MoreOutlined}
            />
        </div>
    );
}
{
    /* <DislikeOutlined /> */
}
