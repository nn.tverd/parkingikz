import React from "react";

export default function MyCarusel({ items, setMainImage }) {
    console.log("console.log", "MyCarusel");
    return (
        <div
            style={{
                minWidth: "100%",
                height: "100%",
                color: "white",
                display: "grid",
                gridTemplateRows: "1fr",
                gridAutoFlow: "column",
                // gridTemplateColumns: "1fr 1fr 1fr",
                placeItems: "center",
                overflowX: "scroll",
                overflowY: "hidden",
                gridAutoColumns: "32%",
                gridGap: 5,
                scrollSnapType: "x mandatory",
            }}
        >
            {items &&
                items.map((item, index) => {
                    return (
                        <div
                            style={{
                                backgroundColor: "orange",
                                width: "100%",
                                height: "100%",
                                scrollSnapAlign: "start",
                            }}
                            key={index}
                            onClick={() => {
                                setMainImage(item.image);
                            }}
                        >
                            <img
                                src={item.image}
                                alt={item.type}
                                style={{
                                    objectFit: "cover",
                                    width: "100%",
                                    height: "100%",
                                }}
                            />
                        </div>
                    );
                })}
            {/* <div
                style={{
                    backgroundColor: "orange",
                    width: "100%",
                    height: "100%",
                    scrollSnapAlign: "start",
                }}
            >
                123
            </div>
            <div
                style={{
                    backgroundColor: "orange",
                    width: "100%",
                    height: "100%",
                    scrollSnapAlign: "start",
                }}
            >
                123
            </div>
            <div
                style={{
                    backgroundColor: "orange",
                    width: "100%",
                    height: "100%",
                    scrollSnapAlign: "start",
                }}
            >
                123
            </div>
            <div
                style={{
                    backgroundColor: "orange",
                    width: "100%",
                    height: "100%",
                    scrollSnapAlign: "start",
                }}
            >
                123
            </div> */}
        </div>
    );
}
