import React from "react";
import Link from "next/link";
import { Layout, Input, Space, Tooltip, Button } from "antd";
import { List, Typography, Divider } from "antd";
import { SearchOutlined, BarsOutlined } from "@ant-design/icons";

const dataCategories = [
    {
        name: "Все объявления",
        href:
            "/all?query=&parking=true&garage=true&pantry=true&minPrice=0&maxPrice=50000000&cash=true&installment=true&loan=false&all=true&only=false",
        icon: <SearchOutlined />,
    },
    {
        name: "Паркинги",
        href:
            "/all?query=&parking=true&garage=false&pantry=false&minPrice=0&maxPrice=50000000&cash=true&installment=true&loan=false&all=true&only=false",

        icon: <SearchOutlined />,
    },
    {
        name: "Гаражи",
        href:
            "/all?query=&parking=false&garage=true&pantry=false&minPrice=0&maxPrice=50000000&cash=true&installment=true&loan=false&all=true&only=false",

        icon: <SearchOutlined />,
    },
    {
        name: "Кладовки",
        href:
            "/all?query=&parking=false&garage=false&pantry=true&minPrice=0&maxPrice=50000000&cash=true&installment=true&loan=false&all=true&only=false",

        icon: <SearchOutlined />,
    },
    // {name: "Паркинги", href:"/list"},
];

const dataControl = [
    {
        name: "Добавить объявление",
        href: "/add",
        icon: <SearchOutlined />,
    },
    { name: "Мои объявления", href: "/parkings", icon: <SearchOutlined /> },
    { name: "Я Риелтор", href: "/garages", icon: <SearchOutlined /> },
    { name: "Настройки", href: "/garages", icon: <SearchOutlined /> },
    { name: "Контакты", href: "/garages", icon: <SearchOutlined /> },
    { name: "Информация", href: "/garages", icon: <SearchOutlined /> },
    { name: "Войти / Выйти", href: "/garages", icon: <SearchOutlined /> },

    // {name: "Паркинги", href:"/list"},
];

export default function NavMenu({ setShowMenu }) {
    return (
        <div
            style={{
                position: "absolute",
                width: "95vw",
                height: "100vh",
                background: "whitesmoke",
                right: 0,
                top: 0,
                overflowY: "scroll",
                zIndex: 2,
            }}
        >
            <div
                style={{
                    width: "100%",
                    display: "grid",
                    justifyContent: "start",
                    padding: 5,
                    position: "sticky",
                }}
            >
                <Button
                    onClick={() => {
                        setShowMenu(false);
                    }}
                >
                    Close
                </Button>
            </div>
            <div style={{ padding: 10, overflowY: "scroll" }}>
                <Divider orientation="left">Категории объектов</Divider>
                <List
                    // header={<div>Header</div>}
                    // footer={<div>Footer</div>}
                    bordered
                    dataSource={dataCategories}
                    renderItem={(item, index) => (
                        <Link href={item.href} key={index}>
                            <a>
                                <List.Item>
                                    <Typography.Text>
                                        {item.icon}
                                    </Typography.Text>{" "}
                                    {item.name}
                                </List.Item>
                            </a>
                        </Link>
                    )}
                />
                <Divider orientation="left">Управление</Divider>
                <List
                    // header={<div>Header</div>}
                    // footer={<div>Footer</div>}
                    bordered
                    style={{ overflowY: scroll }}
                    dataSource={dataControl}
                    renderItem={(item, index) => (
                        <Link href={item.href} key={index}>
                            <a>
                                <List.Item>
                                    <Typography.Text>
                                        {item.icon}
                                    </Typography.Text>{" "}
                                    {item.name}
                                </List.Item>
                            </a>
                        </Link>
                    )}
                />
            </div>
        </div>
    );
}
