import React, { useEffect, useState } from "react";
import { Layout, Input, Space, Tooltip, Button } from "antd";
import { useRouter } from "next/router";
import {
    SearchOutlined,
    ZoomInOutlined,
    HomeOutlined,
    DollarOutlined,
    EnvironmentOutlined,
    GoldOutlined,
    FullscreenOutlined,
    DislikeOutlined,
    HeartOutlined,
    CheckCircleOutlined,
    ShareAltOutlined,
    StarOutlined,
} from "@ant-design/icons";
import globalStore from "../../store/globalStore";
import { observer } from "mobx-react-lite";
// import Rainbowfy from "react-rainbowfy";

const SideBarButton = ({
    fontColor,
    bgColor,
    title,
    showClick,
    showTouch,
    hide,
    Icon,
}) => {
    console.log(fontColor);
    const btnStyle = {
        color: fontColor,
        backgroundColor: bgColor,
        borderColor: bgColor,
    };

    return (
        <div
            style={{
                display: "grid",
                placeItems: "center",
                gridTemplateColumns: "1fr",
            }}
        >
            <Button
                shape="round"
                size="middle"
                style={btnStyle}
                icon={<Icon />}
                // onClick={() => setShowSearch(true)}
                onTouchStart={() => showTouch()}
                onMouseDown={() => showClick()}
                onTouchEnd={() => hide()}
                onMouseUp={() => {
                    hide();
                }}
            />
            <div
                style={{
                    display: "grid",
                    placeItems: "center",
                    width: "100%",
                }}
            >
                {/* <Rainbowfy> */}
                <strong className="tiktok-glithc-h1">{title}</strong>
                {/* </Rainbowfy> */}
            </div>
        </div>
    );
};
const emptyFunction = () => {};
const likes = 7;
export default observer(function ListBotBar({ parking }) {
    const router = useRouter();
    const {
        showHowMany,
        hideHowMany,
        isShowMoreVisible,
    } = globalStore.sideBarStore;

    const [like, setLike] = useState(false);

    console.log("bye", isShowMoreVisible);

    useEffect(() => {
        console.log("side bar use effect");
        const test = window.sessionStorage.getItem("test") || 0;
        console.log(test);
        window.sessionStorage.setItem("test", Number(test) + 1);
    }, []);
    return (
        <div
            style={{
                position: "absolute",
                width: "70%",
                left: 10,
                bottom: 10,
                // backgroundColor: "red",
                display: "grid",
                gridTemplateColumns: "1fr 1fr 1fr 1fr",
                placeItems: "center",
                gridGap: 10,
                zIndex: 1,
            }}
        >
            {isShowMoreVisible && (
                <>
                    <SideBarButton
                        fontColor="white"
                        bgColor="black"
                        title="Доступно"
                        showTouch={showHowMany}
                        showClick={emptyFunction}
                        hide={hideHowMany}
                        Icon={GoldOutlined}
                    />

                    <SideBarButton
                        fontColor="white"
                        bgColor="#111"
                        title="Скрыть"
                        showClick={emptyFunction}
                        showTouch={emptyFunction}
                        hide={emptyFunction}
                        Icon={DislikeOutlined}
                    />
                </>
            )}
            <SideBarButton
                fontColor="white"
                bgColor="black"
                title="Другу"
                showTouch={emptyFunction}
                showClick={emptyFunction}
                hide={hideHowMany}
                Icon={ShareAltOutlined}
            />
            <SideBarButton
                fontColor="white"
                bgColor={like ? "#f00" : "#300"}
                title={like ? likes + 1 : likes}
                showClick={() => {
                    setLike(!like);
                }}
                showTouch={emptyFunction}
                hide={emptyFunction}
                Icon={HeartOutlined}
            />
            <SideBarButton
                fontColor="white"
                bgColor="#000"
                title="Избранное"
                showClick={emptyFunction}
                showTouch={emptyFunction}
                hide={emptyFunction}
                Icon={StarOutlined}
            />
            <SideBarButton
                fontColor="white"
                bgColor="#030"
                title="Выбрать"
                showClick={emptyFunction}
                showTouch={emptyFunction}
                hide={() => {
                    router.push(`/obj/${parking._id}`);
                }}
                Icon={CheckCircleOutlined}
            />
        </div>
    );
});
