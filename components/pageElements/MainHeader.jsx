import React, { useState } from "react";
import { Router, useRouter } from "next/router";
import { Layout, Input, Space, Tooltip, Button } from "antd";
import { List, Typography, Divider } from "antd";
import {
    SearchOutlined,
    BarsOutlined,
    PlusCircleOutlined,
    PlusOutlined,
    SendOutlined,
} from "@ant-design/icons";
import SearchMenu from "./SearchMenu";
import NavMenu from "./NavMenu";
// import { ReactComponent as ReactLogo } from "../../public/some.svg";
// import Logo from "../../public/some.svg";
// const { Header, Footer, Content } = Layout;
// const { Search } = Input;

export default function MainHeader() {
    const [showSearch, setShowSearch] = useState(false);
    const [showMenu, setShowMenu] = useState(false);
    const router = useRouter();
    return (
        <>
            <div
                style={{
                    backgroundColor: "black",
                    color: "white",
                    // position: "fixed",
                    zIndex: 3,
                    width: "100%",
                    height: "50px",
                    position: "fixed",
                    bottom: 0,
                    left: 0,
                    display: "grid",
                    gridTemplateColumns: "1fr 1fr 1fr 1fr 1fr",
                    placeItems: "center",
                }}
            >
                <div>
                    <img src="/some.svg" alt="logo" />
                </div>
                <div>
                    <Tooltip title="search">
                        <Button
                            shape="circle"
                            icon={<SearchOutlined />}
                            onClick={() => setShowSearch(true)}
                        />
                    </Tooltip>
                </div>
                <div>
                    <Tooltip title="Новое объявление">
                        <Button
                            shape="circle"
                            style={{
                                color: "white",
                                backgroundColor: "black",
                            }}
                            icon={<PlusOutlined />}
                            onClick={() => router.push("/add")}
                        />
                    </Tooltip>
                </div>
                <div>
                    <Tooltip title="menu">
                        <Button
                            shape="circle"
                            icon={<BarsOutlined />}
                            onClick={() => setShowMenu(true)}
                        />
                    </Tooltip>
                </div>
                <div>
                    <Tooltip title="menu">
                        <Button
                            shape="circle"
                            icon={<SendOutlined />}
                            onClick={() => setShowMenu(true)}
                        />
                    </Tooltip>
                </div>
            </div>
            {showMenu && <NavMenu setShowMenu={setShowMenu} />}
            {showSearch && <SearchMenu setShowSearch={setShowSearch} />}
        </>
    );
}
