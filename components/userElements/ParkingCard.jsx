import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import { Col, Row, Button, DatePicker, version, Badge } from "antd";
import ListSideBar from "../pageElements/ListSideBar";
import globalStore from "../../store/globalStore";
import { observer } from "mobx-react-lite";
import ParkingCardDetails from "../pieces/ParkingCardDetails";
import MyCarusel from "../pageElements/MyCarusel";
import ListBotBar from "../pageElements/ListBotBar";

// import DnsIcon from "@material-ui/icons/Dns";
// import { Divider } from "antd";
import {
    InfoCircleOutlined,
    StarOutlined,
    EyeOutlined,
    CheckSquareOutlined,
    SearchOutlined,
    ZoomInOutlined,
    HomeOutlined,
    DollarOutlined,
    EnvironmentOutlined,
    GoldOutlined,
    FullscreenOutlined,
    DislikeOutlined,
    HeartOutlined,
    CheckCircleOutlined,
    DiffOutlined,
} from "@ant-design/icons";

// const items = [
//     {
//         name: "house view",
//         image: "https://picsum.photos/800/600?random=1",
//     },
//     {
//         name: "parking view",
//         image: "https://picsum.photos/800/600?random=2",
//     },
//     {
//         name: "map view",
//         image: "https://picsum.photos/800/600?random=3",
//     },
//     {
//         name: "house view",
//         image: "https://picsum.photos/800/600?random=1",
//     },
//     {
//         name: "parking view",
//         image: "https://picsum.photos/800/600?random=2",
//     },
//     {
//         name: "map view",
//         image: "https://picsum.photos/800/600?random=3",
//     },
//     {
//         name: "house view",
//         image: "https://picsum.photos/800/600?random=1",
//     },
//     {
//         name: "parking view",
//         image: "https://picsum.photos/800/600?random=2",
//     },
//     {
//         name: "map view",
//         image: "https://picsum.photos/800/600?random=3",
//     },
// ];
const ParkingCard = ({ parking }) => {
    const {
        isPriceVisible,
        isAddressVisible,
        isSizeVisible,
        isHowManyVisible,
    } = globalStore.sideBarStore;
    const [mainImage, setMainImage] = useState("");
    const [hideBottomBar, setHideBottomBar] = useState(false);
    const [showCoverToRead, setShowCoverToRead] = useState(false);

    const items = parking.images;
    useEffect(() => {
        console.log("if (items.length) {");
        if (items.length) {
            setMainImage(items[0].image);
        }
    }, []);
    useEffect(() => {
        setHideBottomBar(isPriceVisible | isAddressVisible | isSizeVisible);
    }, [isPriceVisible, isAddressVisible, isSizeVisible]);

    const minPrice = parking.minPrice
        .toString()
        .replace(/(\d)(?=(\d{3})+(\.(\d){0,2})*$)/g, "$1 ");
    return (
        <div
            style={{
                // opacity:0.1,
                position: "relative",
                height: "100%",
                borderTop: "1px solid gray",
                width: "100%",
                scrollSnapAlign: "start",
                scrollSnapStop: "always",
                display: "grid",
                // alignItems: "center",
                // gridAutoRows: "auto 33% 33%",
                // gridAutoFlow: "dense",
                gridTemplateColumns: "1fr 1fr 1fr",
                gridTemplateRows: "4fr 1fr",
                gridGap: "0.3rem",
            }}
        >
            <div
                style={{
                    // backgroundColor: "orange",
                    gridColumn: "1/4",
                    gridRow: "1/1",
                    overflow: "hidden",
                    position: "relative",
                }}
                // onMouseDown={() => {
                //     setShowCoverToRead(true);
                // }}
                // onMouseUp={() => {
                //     setShowCoverToRead(false);
                // }}
            >
                <ListSideBar />
                {!hideBottomBar && <ListBotBar parking={parking} />}
                <div
                    style={{
                        position: "absolute",
                        bottom: 10,
                        right: 10,
                        zIndex: 100,
                    }}
                >
                    <Button
                        shape="circle"
                        size="middle"
                        icon={<EyeOutlined />}
                        style={{
                            backgroundColor: "lightgray",
                            borderColor: "lightgray",
                            color: "black",
                            // width: 50,
                        }}
                        onMouseDown={() => {
                            setShowCoverToRead(true);
                        }}
                        onTouchStart={() => {
                            setShowCoverToRead(true);
                        }}
                        onMouseUp={() => {
                            setShowCoverToRead(false);
                        }}
                        onTouchEnd={() => {
                            setShowCoverToRead(false);
                        }}
                    />
                </div>

                <img
                    // src="https://photos-kr.kcdn.kz/content/f7/65154ec4a728407e-01.jpg"
                    src={mainImage}
                    alt="complex"
                    style={{
                        width: "100%",
                        height: "100%",
                        objectFit: "cover",
                    }}
                />
                {showCoverToRead && (
                    <div
                        style={{
                            position: "absolute",
                            backgroundColor: "gray",
                            height: "100%",
                            width: "100%",
                            top: 0,
                            left: 0,
                            // zIndex: 1,
                            opacity: 0.8,
                        }}
                    ></div>
                )}
                <div
                    style={{
                        width: "90%",
                        height: "70vh",
                        // backgroundColor: "orange",
                        position: "absolute",
                        top: 0,
                        left: 0,
                        // zIndex: 5,
                        display: "grid",
                        alignContent: "start",
                    }}
                >
                    <div
                        style={{
                            display: "grid",
                            placeItems: "center",
                            width: "100%",
                            gridTemplateColumns: "6fr 13fr 3fr",
                            paddingTop: 15,
                            paddingBottom: 10,
                        }}
                    >
                        <div></div>
                        <div>
                            <Button
                                // shape="circle"
                                size="middle"
                                icon={<InfoCircleOutlined />}
                                style={{
                                    borderRadius: "15px 0px 0px 15px",
                                    width: 50,
                                    borderColor: "black",
                                    backgroundColor: "black",
                                    color: "white",
                                }}
                            />
                            <Badge
                                count={5}
                                style={{
                                    // zIndex: -1,
                                    border: "none",
                                }}
                            >
                                <Button
                                    // shape="circle"
                                    size="middle"
                                    icon={<StarOutlined />}
                                    style={{
                                        borderRadius: "0px 15px 15px 0px",
                                        width: 50,
                                    }}
                                />
                            </Badge>
                        </div>
                    </div>
                    <ParkingCardDetails
                        text1={parking.address}
                        text2={parking.name}
                        switcher={isAddressVisible}
                    />
                    <ParkingCardDetails
                        text1={`от ${minPrice}`}
                        text2="тенге"
                        switcher={isPriceVisible}
                    />
                    <ParkingCardDetails
                        text1={`${(5, 5)} X ${3} X ${3} X ${16.5}`}
                        text2="Габариты (Длина Х Ширина Х Высота Х Площадь)"
                        switcher={isSizeVisible}
                    />
                    <ParkingCardDetails
                        text1={`${Math.round(10 * Math.random())} шт`}
                        text2="количество в продаже"
                        switcher={isHowManyVisible}
                    />
                </div>
            </div>
            <div
                style={{
                    // backgroundColor: "red",
                    gridColumn: "1/4",
                    gridRow: "2/2",
                    // borderTop: "3px solid whitesmoke",
                    // borderRight: "3px solid whitesmoke",
                    overflow: "hidden",
                }}
            >
                {/* <img
                    src="https://static.legenda-dom.ru/content/dalnevostochny/slideshow/sme09683_min..jpg"
                    alt="complex"
                    style={{
                        width: "100%",
                        height: "100%",
                        objectFit: "cover",
                    }}
                /> */}
                <MyCarusel
                    items={items}
                    setMainImage={setMainImage}
                ></MyCarusel>
            </div>
            {/* <div
                style={{
                    // backgroundColor: "blue",
                    gridColumn: "2/2",
                    gridRow: "2/2",
                    // borderTop: "3px solid whitesmoke",
                    // borderLeft: "3px solid whitesmoke",
                    overflow: "hidden",
                }}
            >
                <img
                    src="https://st2.depositphotos.com/3895411/8390/i/950/depositphotos_83903146-stock-photo-astana-pinned-on-a-map.jpg"
                    alt="complex"
                    style={{
                        width: "100%",
                        height: "100%",
                        objectFit: "cover",
                    }}
                />
            </div> */}
        </div>
    );
};
// export default ParkingCard;

export default observer(ParkingCard);
