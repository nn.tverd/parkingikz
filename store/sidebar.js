import { makeAutoObservable } from "mobx";

class sideBar {
    isPriceVisible = false;
    isAddressVisible = false;
    isSizeVisible = false;
    isHowManyVisible = false;
    isShowMoreVisible = false;
    debounceTimer = null;
    // someStore;
    constructor(rootStore) {
        // this.someStore = rootStore.someStore;
        console.log(1);
        makeAutoObservable(this);
    }

    showPrice = () => {
        console.log("showPrice = () => {");
        this.isPriceVisible = true;
    };
    hidePrice = () => {
        console.log("hidePrice = () => {");
        this.isPriceVisible = false;
    };

    showAddress = () => {
        this.isAddressVisible = true;
    };
    hideAddress = () => {
        this.isAddressVisible = false;
    };

    showSize = () => {
        this.isSizeVisible = true;
    };
    hideSize = () => {
        this.isSizeVisible = false;
    };

    showHowMany = () => {
        this.isHowManyVisible = true;
    };
    hideHowMany = () => {
        this.isHowManyVisible = false;
    };
    changeShowMore = () => {
        if (this.debounceTimer) {
            clearTimeout(this.debounceTimer);
            // console.log("this.debounceTimer", this.debounceTimer)
            this.debounceTimer = null;
        }
        this.debounceTimer = setTimeout(() => {
            // console.log("hi", this.isShowMoreVisible);
            this.isShowMoreVisible = !this.isShowMoreVisible;
        }, 400);
    };
}

export default sideBar;
