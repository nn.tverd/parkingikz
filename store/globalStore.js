import { makeAutoObservable } from "mobx";

import sideBar from "./sidebar";
import adminNavigationStore from "./adminNavigationStore";
// global parent store
class GlobalStore {
    sideBarStore;
    adminNavigationStore;

    constructor() {
        makeAutoObservable(this);
        this.sideBarStore = new sideBar(this);
        this.adminNavigationStore = new adminNavigationStore(this);
        // ... another stores
    }
}

export default new GlobalStore();
// // access in child
// class UserStore {
//     constructor(rootStore) {
//         this.routerStore = rootStore.routerStore;
//     }

//     // ...
// }

// // In root component:
// <MobxProvider {...new GlobalStore()}>
//   <App />
// </MobxProvider>
