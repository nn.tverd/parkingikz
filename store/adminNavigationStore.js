import { makeAutoObservable } from "mobx";

class adminNavigationStore {
    createSubMenu; // OBJECT, OFFER
    moderSubMenu;  // OBJECT, OFFER
    // someStore;
    constructor(rootStore) {
        // this.someStore = rootStore.someStore;
        makeAutoObservable(this);
        this.selectedSubMenu = "OBJECT";
        this.moderSubMenu = "OBJECT";

    }

    selectCreate = (selectedSubMenu) => {
        this.createSubMenu = selectedSubMenu;
    };
    selectModer = (selectedSubMenu) => {
        this.moderSubMenu = selectedSubMenu;
    };
}

export default adminNavigationStore;
