const mongoose = require("mongoose");
const { Schema } = mongoose;

const OfferSchema = Schema(
    {
        object: {
            type: mongoose.Types.ObjectId,
            ref: "objects",
        },

        name: { type: String, required: true }, // nomer parkovki, nomer garaja,
        level: { type: String, required: true }, // uroven ili ataj
        totalLevel: { type: String, required: true }, // vsego urovney // esli vsego 1 uroven, to ne pokazyvat'

        // sizes
        description: { type: String, default: "" },

        type: { type: String, required: true }, // PARKING, GARAGE, PANTRY
        priceBasic: { type: Number, required: true },

        nameInternal: { type: String, default: "" },
        isActive: { type: Boolean, default: false },
        isModerated: { type: Boolean, default: false },
        likes: { type: Number, default: 0 },

        images: [
            {
                image: { type: String },
                type: { type: String, defalult: "photo" },
            },
        ],
        nameDrafted: { type: String, default: "" },
        descriptionDrafted: { type: String, default: "" },
        imagesDrafted: [
            {
                image: { type: String },
                type: { type: String, defalult: "photo" },
            },
        ],
    },
    {
        timestamp: true,
    }
);

module.exports = mongoose.models["Offer"] || mongoose.model("Offer", OfferSchema, "offers");
