const mongoose = require("mongoose");
const { Schema } = mongoose;

const BuildingScema = Schema(
    {
        name: { type: String, required: true },
        address: { type: String, required: true },
        cityRegion: { type: String },
        zip: { type: String },

        mapSaved2gis: { type: String },
        mapSavedGoogle: { type: String },
        mapSavedYandex: { type: String },

        long: {
            type: Number,
            // required: true
        },
        lat: {
            type: Number,
            // required: true
        },

        city: { type: String, required: true },
        country: { type: String, required: true },

        isActive: { type: Boolean, default: false },
        isModerated: { type: Boolean, default: false },
        minPrice: { type: Number, default: 0 },
        activeItemCount: { type: Number, default: 0 },
        offers: [
            {
                type: mongoose.Types.ObjectId,
                ref: "offer",
            },
        ],
        images: [
            {
                image: { type: String },
                type: { type: String, defalult: "photo" },
            },
        ],
        likes: { type: Number, default: 0 },
        nameDrafted: { type: String, required: true },
        addressDrafted: { type: String, required: true },
        cityRegionDrafted: { type: String },
        cityDrafted: { type: String, required: true },
        countryDrafted: { type: String, required: true },
        longDrafted: { type: Number, required: true },
        latDrafted: { type: Number, required: true },
        imagesDrafted: [
            {
                image: { type: String },
                type: { type: String, defalult: "photo" },
            },
        ],
    },
    {
        timestamp: true,
    }
);

module.exports =
    mongoose.models["Building"] ||
    mongoose.model("Building", BuildingScema, "objects");
