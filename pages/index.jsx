import { Button, DatePicker, version } from "antd";
import "antd/dist/antd.css";
import { useEffect, useState } from "react";
// import { DatePicker } from "antd-mobile";
import MainLayout from "../components/Layouts/MainLayout.jsx";
// import sideBar from "../store/sidebar.js";

// import globalStore from "../store/globalStore.js";
// import ParkingCard from "../components/userElements/ParkingCard.jsx";

import styles from "../styles/Home.module.css";
import { dbConnect, jsonify } from "../middleware/db.js";

import Building from "../models/building";

// https://www.npmjs.com/package/react-textscramble

export async function getServerSideProps(ctx) {
    await dbConnect();
    const buildings = await Building.find();
    console.log(buildings);
    return {
        props: {
            fighters: [{ name: "connor mcgregor" }],
            buildings: jsonify(buildings),
        },
    };
}
// export async function getServerSideProps(ctx) {

//     dbConnect()
//     const buildings = await fetch("http://localhost:3000/api/buildings");
//     return {
//         props: {
//             fighters: [{ name: "connor mcgregor" }],
//             buildings: jsonify(buildings),
//         },
//     };
// }

export default function Home({ fighters, buildings }) {
    console.log(buildings);
    return (
        <MainLayout>
            <main className={styles.main}>
                <img src="/some.png" alt="logo" />
                <div>
                    <h3
                        style={{
                            color: "white",
                        }}
                    >
                        Используйте кнопку поиска
                    </h3>
                </div>
            </main>
        </MainLayout>
    );
}
