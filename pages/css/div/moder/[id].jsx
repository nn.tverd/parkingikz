import { observer } from "mobx-react-lite";
import { useRouter } from "next/router";

import AdminLayout from "../../../../components/Layouts/AdminLayout";
import { Button, Collapse } from "antd";

const { Panel } = Collapse;

function callback(key) {
    console.log(key);
}

const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

export default observer(function EditObject() {
    const router = useRouter();
    console.log(router.query.id);
    return (
        <AdminLayout>
            <h3>Edit object</h3>
            <Collapse defaultActiveKey={["1", "2"]} onChange={callback}>
                <Panel header="Название объекта и адрес" key="1">
                    <p>{text}</p>
                    <Button>Save</Button>
                    <Button>Close</Button>

                </Panel>
                <Panel header="This is panel header 2" key="4">
                    <p>{text}</p>
                </Panel>
                <Panel header="This is panel header 3" key="3">
                    <p>{text}</p>
                </Panel>
            </Collapse>
        </AdminLayout>
    );
});
