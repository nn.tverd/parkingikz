import MainLayout from "../components/Layouts/MainLayout.jsx";

export default function Home() {
    return (
        <MainLayout>
            <h1 style={{ color: "white" }}>Here you can add object to sell</h1>
        </MainLayout>
    );
}
