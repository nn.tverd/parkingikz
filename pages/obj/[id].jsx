import { Button } from "antd";
import MainLayout from "../../components/Layouts/MainLayout";
import { StarOutlined } from "@ant-design/icons";

function DetailsObject({ object }) {
    // console.log(object);
    return (
        <MainLayout>
            <div
                style={{
                    width: "100%",
                    backgroundColor: "white",
                }}
            >
                <div
                    style={{
                        backgroundColor: "whitesmoke",
                        padding: 20,
                    }}
                >
                    <h2
                        style={{
                            color: "black",
                        }}
                    >
                        {object.address}
                    </h2>
                    <div
                        style={{
                            display: "grid",
                            placeItems: "center",
                            gridTemplateColumns: "4fr 1fr",
                        }}
                    >
                        <div style={{ width: "100%" }}>
                            <h1>{object.name}</h1>
                        </div>
                        <div>
                            <Button
                                shape="circle"
                                size="large"
                                // style={btnStyle}
                                icon={<StarOutlined />}
                                // onClick={() => setShowSearch(true)}
                                onTouchStart={() => {}}
                                onMouseDown={() => {}}
                                onTouchEnd={() => {}}
                                onMouseUp={() => {}}
                            />
                        </div>
                    </div>
                    <div>
                        <Button
                            style={{
                                width: "100%",
                            }}
                            type="default"
                        >
                            Заявка на покупку
                        </Button>
                    </div>
                </div>
                <div>
                    <h1
                        style={{
                            padding: 20,
                        }}
                    >
                        Фотографии
                    </h1>
                    {object &&
                        object.images &&
                        object.images.map((image, index) => {
                            return (
                                <div key={index}>
                                    <img
                                        src={image.image}
                                        alt={image.type}
                                        width="100%"
                                    />
                                </div>
                            );
                        })}
                </div>
                <div
                    style={{
                        padding: 20,
                    }}
                >
                    <h1>Посмотреть на карте</h1>
                    <div>
                        <Button>2gis</Button>
                        <Button>Google</Button>
                        <Button>Яндекс</Button>
                    </div>
                </div>
                <div
                    style={{
                        padding: 20,
                    }}
                >
                    <h1>Доступные места</h1>
                    <div
                        style={{
                            display: "grid",
                            placeItems: "center",
                            gridGap: 3,
                            gridTemplateColumns: "1fr 1fr 1fr 1fr 1fr",
                        }}
                    >
                        <Button>1</Button>
                        <Button>111</Button>
                        <Button>333</Button>
                    </div>
                    <div>
                        <p
                            style={{
                                marginTop: 10,
                                fontSize: "0.8rem",
                                color: "gray",
                            }}
                        >
                            Выберете место, чтобы получить более подробную
                            информацию
                        </p>
                    </div>
                </div>
                <div
                    style={{
                        padding: 20,
                    }}
                >
                    <h1>Средние размеры</h1>
                    <div
                        style={{
                            display: "grid",
                            placeItems: "center",
                            gridTemplateColumns: "1fr 1fr",
                            // padding: 5,
                            gridGap: 5,
                        }}
                    >
                        <div
                            style={{
                                // border: "1px solid blue",
                                display: "grid",
                                placeItems: "center",
                                backgroundColor: "whitesmoke",
                                width: "100%",
                                height: "100%",
                                paddingTop: 20,
                            }}
                        >
                            <h3>Длина</h3>
                            <h2>5 м</h2>
                        </div>
                        <div
                            style={{
                                // border: "1px solid blue",
                                display: "grid",
                                placeItems: "center",
                                backgroundColor: "whitesmoke",
                                width: "100%",
                                height: "100%",
                                paddingTop: 20,
                            }}
                        >
                            <h3>Ширина</h3>
                            <h2>3 м</h2>
                        </div>
                        <div
                            style={{
                                // border: "1px solid blue",
                                display: "grid",
                                placeItems: "center",
                                backgroundColor: "whitesmoke",
                                width: "100%",
                                height: "100%",
                                paddingTop: 20,
                            }}
                        >
                            <h3>Высота потолка</h3>
                            <h2>3 м</h2>
                        </div>
                        <div
                            style={{
                                // border: "1px solid blue",
                                display: "grid",
                                placeItems: "center",
                                backgroundColor: "whitesmoke",
                                width: "100%",
                                height: "100%",
                                paddingTop: 20,
                            }}
                        >
                            <h3>Площадь</h3>
                            <h2>15 м2</h2>
                        </div>
                    </div>
                </div>
                <div
                    style={{
                        padding: 20,
                    }}
                >
                    <h1>Информация</h1>
                    <p>
                        Информация о ЖК, Его преимущества, стоит ли объект
                        отдельно
                    </p>
                </div>

                <div
                    style={{
                        padding: 20,
                    }}
                >
                    <h1>Условия покупки</h1>
                    <div>
                        <p>Отоимость от 1,5 млн</p>
                        <p>Есть места в рассрочку</p>
                        <p>Есть места в в кредит</p>
                        <p>Есть срочные предложения</p>
                        <p>Специальные предложения</p>
                    </div>
                    <div>
                        <Button
                            style={{
                                width: "100%",
                            }}
                            type="primary"
                        >
                            Заявка на покупку
                        </Button>
                    </div>
                </div>
            </div>
        </MainLayout>
    );
}

// export async function getInitialProps(ctx) {
DetailsObject.getInitialProps = async (ctx) => {
    const response = await fetch(
        "https://my-json-server.typicode.com/nntverd-carcenter/fakeserverparking/objects"
    );
    const __objects = await response.json();
    // console.log("__objects:", __objects, ctx.query.id);

    const object = __objects.find((a) => {
        // console.log("a._id === ctx.query.id", a._id, ctx.query.id);
        return a._id.toString() === ctx.query.id;
    });

    console.log("object:", object);
    return {
        object,
        // props: {
        //     object,
        // },
    };
};

// export async function getStaticPaths() {
//     return {
//         paths: [
//             // String variant:
//             // "/obj/1",
//             // Object variant:
//             { params: { id: "1" } },
//         ],
//         fallback: true,
//     };
// }

export default DetailsObject;
