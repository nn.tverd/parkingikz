import "../styles/globals.css";
import "../styles/animation.css";
// import { MobxProvider } from "mobx-react-lite";
// import GlobalStore from "../store/globalStore.js";


function MyApp({ Component, pageProps }) {
    // console.log(pageProps);
    return (
        // <MobxProvider {...new GlobalStore()}>
        <Component {...pageProps} />
        // </MobxProvider>
    );
}

export default MyApp;
