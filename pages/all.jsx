import { useEffect, useState } from "react";
import MainLayout from "../components/Layouts/MainLayout";
import ParkingCard from "../components/userElements/ParkingCard";
import "antd/dist/antd.css";
import styles from "../styles/Home.module.css";

export default () => {
    const [parkings, setParkings] = useState([]);
    useEffect(async () => {
        const response = await fetch("/api/buildings");
        const __parkings = await response.json();
        console.log(__parkings);
        setParkings(__parkings.result);
    }, []);
    console.log("console.log", "Parkings", parkings);
    return (
        <MainLayout>
            {/* <main className={styles.main}> */}

            {parkings &&
                parkings.map((parking, index) => {
                    return (
                        <ParkingCard
                            key={index}
                            parking={parking}
                        ></ParkingCard>
                    );
                })}

            {/* </main> */}
        </MainLayout>
    );
};
