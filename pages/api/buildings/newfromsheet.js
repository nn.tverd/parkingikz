import createHandler from "../../../middleware/middleware";
import Building from "../../../models/building";
import Offer from "../../../models/offer";

const handler = createHandler();

handler.post(async (req, res) => {
    // poluchit spisok ob'ektov
    console.log("post newfromsheet object/");
    try {
        const {
            name,
            address,
            cityId,
            long,
            lat,
            images,
            zip,
            cityRegion,
            mapSaved,
        } = req.body;

        console.log(
            name,
            address,
            cityId,
            long,
            lat,
            images,
            zip,
            cityRegion,
            mapSaved
        );
        if (!cityId) throw "city id must be in request";
        const _city = await City.findOne({ _id: cityId });
        if (!_city) throw "city with this id is not found";
        const { city, country } = _city;

        console.log(_city, city, country);

        const maps = {
            mapSaved2gis: "",
            mapSavedGoogle: "",
            mapSavedYandex: "",
        };
        if (mapSaved) {
            if (mapSaved.includes("2gis")) {
                maps.mapSaved2gis = mapSaved;
            }
            if (mapSaved.includes("google")) {
                maps.mapSavedGoogle = mapSaved;
            }
            if (mapSaved.includes("yandex")) {
                maps.mapSavedYandex = mapSaved;
            }
        }

        const newObject = new Object({
            ...maps,
            name,
            address,

            cityRegion,
            zip,

            city,
            country,
            long: long,
            lat: lat,

            minPrice: 0,
            activeItemCount: 0,
            likes: 0,

            nameDrafted: name,
            addressDrafted: address,
            cityRegionDrafted: cityRegion,
            cityDrafted: city,
            countryDrafted: country,
            longDrafted: long,
            latDrafted: lat,
            imagesDrafted: [...images],
        });
        await newObject.save();
        return res.status(200).json({
            message: "object added successfully",
            result: newObject,
        });
    } catch (error) {
        const code = Math.random() * 100000;
        console.log(error, code);
        res.status(500).json({
            message: "Internal server error",
            codeoferror: code,
        });
    }
});

handler.post(async (req, res) => {
    const buildings = await Building.find();
    res.status(200).json({ bldgs: buildings });
});

export default handler;
