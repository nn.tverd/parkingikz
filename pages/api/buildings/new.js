import createHandler from "../../../middleware/middleware";
import Building from "../../../models/building";
import Offer from "../../../models/offer";

const handler = createHandler();

handler.post(async (req, res) => {
    // poluchit spisok ob'ektov
    console.log("post object/");
    try {
        const { name, address, city, country, long, lat, images } = req.body;
        console.log(name, address, city, country, long, lat, images);
        const newBuilding = new Building({
            name,
            address,
            city,
            country,
            long: long,
            lat: lat,

            minPrice: 0,
            activeItemCount: 0,
            likes: 0,

            nameDrafted: name,
            addressDrafted: address,
            cityDrafted: city,
            countryDrafted: country,
            longDrafted: long,
            latDrafted: lat,
            imagesDrafted: [...images],
        });
        await newBuilding.save();
        return res.status(200).json({
            message: "building added successfully",
            result: newBuilding,
        });
    } catch (error) {
        const code = Math.random() * 100000;
        console.log(error, code);
        res.status(500).json({
            message: "Internal server error",
            codeoferror: code,
        });
    }
});

handler.post(async (req, res) => {
    const buildings = await Building.find();
    res.status(200).json({ bldgs: buildings });
});

export default handler;
