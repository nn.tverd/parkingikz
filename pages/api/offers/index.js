import createHandler from "../../../middleware/middleware";
import Offer from "../../../models/offer";
// import Offer from "../../../models/offer";

const handler = createHandler();

handler.get(async (req, res) => {
    // poluchit spisok ob'ektov
    console.log("get cities/");
    try {
        const offers = await Offer.find();
        res.json({
            message: "list of offers loaded successfully",
            result: offers,
        });
    } catch (err) {
        res.json({ message: err });
    }
});

handler.post(async (req, res) => {
    const buildings = await Building.find();
    res.status(200).json({ bldgs: buildings });
});

export default handler;
