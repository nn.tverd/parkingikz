import createHandler from "../../../middleware/middleware";
import Building from "../../../models/building";
import Offer from "../../../models/offer";
import mongoose from "mongoose";
const handler = createHandler();

handler.post(async (req, res) => {
    console.log("offer/new");
    try {
        const {
            objectId,
            description,
            type,
            priceBasic,
            nameInternal,
            level,
            name,
            totalLevel,
            images,
        } = req.body;

        const object = await Object.findOne({
            _id: mongoose.Types.ObjectId(objectId),
        });

        if (!object || !object._id) throw "object with this ID is not found";

        const newOffer = new Offer({
            object: object._id,
            description,
            name,
            type,
            level,
            totalLevel,
            priceBasic,
            nameInternal,

            descriptionDrafted: description,
            nameDrafted: name,
            imagesDrafted: [...images],
        });
        await newOffer.save();

        object.offers.push(newOffer._id);
        await object.save();

        console.log(object);

        return res.status(200).json({
            message: "offer added successfully",
            result: newOffer,
        });
    } catch (error) {
        const code = Math.random() * 100000;
        console.log(error, code);
        res.status(500).json({
            message: "Internal server error",
            codeoferror: code,
        });
    }
});

handler.post(async (req, res) => {
    const buildings = await Building.find();
    res.status(200).json({ bldgs: buildings });
});

export default handler;
